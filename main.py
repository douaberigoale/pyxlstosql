import glob

from FileProcessor import FileProcessor
from TableInformation import TableInformation

input_files = glob.iglob("./input/*.xlsx")

for xls_path in input_files:
    table_name = "`product`"
    table_name_special = "product_special ps JOIN product p ON ps.product_id = p.product_id"
    column_name = "`price`"
    code_name = "`model`"
        
    information = TableInformation(xls_path,
                                   table_name,
                                   table_name_special,
                                   column_name,
                                   code_name)

    FileProcessor(information).create_updates(xls_path)