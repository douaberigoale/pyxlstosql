class UpdateStatement:

    def __init__(self, table_name, column_name, new_value, condition_column, condition_value):
        self.table_name = table_name
        self.column_name = column_name
        self.new_value = new_value
        self.condition_column = condition_column
        self.condition_value = condition_value

    def generate_query(self):
        return "UPDATE {0} SET {1} = {2} WHERE {3} in {4};" \
            .format(self.table_name, self.column_name, self.new_value, self.condition_column, self.generate_in())

    def generate_query_no_enumerate(self):
        return "UPDATE {0} SET {1} = {2} WHERE {3} = '{4}';" \
            .format(self.table_name, self.column_name, self.new_value, self.condition_column, self.condition_value)

    def generate_join_query_for_sets(self):
        return "UPDATE {0} SET ps.{1} = {2} WHERE p.{3} = '{4}';" \
            .format(self.table_name, self.column_name, self.new_value, self.condition_column, self.condition_value)

    def generate_in(self):
        result = '('
        for index, condition in enumerate(self.condition_value):
            result = result + '\'' + condition + '\''

            if index < len(self.condition_value) - 1:
                result = result + ', '

        result = result + ')'
        return result

