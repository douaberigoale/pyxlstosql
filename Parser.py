# Import pandas
import pandas as pd


class Parser:

    def __init__(self, input_xls, output_file):
        self.input_xls = input_xls
        self.output_file = output_file

    def parse_simple(self, i: int):
        return pd.read_excel(self.input_xls, skiprows=i)

    def write_to_file(self, string_to_be_written):
        with open(self.output_file, "a+") as working_file:
            working_file.write("\n\n %s" % string_to_be_written)
