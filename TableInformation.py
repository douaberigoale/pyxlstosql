import os


class TableInformation:

    def __init__(self, output_file_name, table_name, table_name_special, column_name, code_name):
        self.output_file_name = output_file_name.replace("xlsx", "txt").replace("input", "output")
        self.table_name = table_name
        self.table_name_special = table_name_special
        self.column_name = column_name
        self.code_name = code_name
