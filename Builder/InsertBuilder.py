class InsertBuilder:

    statement: str = "INSERT INTO "

    def __init__(self):
        pass

    class WithTable:
        def __init__(self, table_name):
            InsertBuilder.statement += table_name

        class InsertIntoColumns:
            def __init__(self, columns):
                InsertBuilder.statement += "(" + columns + ")"

            class Values:
                def __init__(self, values):
                    InsertBuilder.statement += " VALUES "
                    InsertBuilder.statement += "(" + values + ")"

                class Build:
                    def __new__(cls):
                        return InsertBuilder.statement