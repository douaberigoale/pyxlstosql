from Parser import Parser
from TableInformation import TableInformation
from UpdateStatement import UpdateStatement


def is_nan(num):
    return num != num


def is_float(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


def price_is_not_as_expected(price_row):
    return is_nan(price_row) or not is_float(price_row)


class FileProcessor:

    def __init__(self, info: TableInformation):
        self.info = info

    def create_updates(self, xls_path: str):
        parser = Parser(xls_path, self.info.output_file_name)

        if "special" in xls_path:
            self.create_special_price_updates(parser)
        else:
            self.create_price_updates_from_original_anita_files(parser)

    def create_price_updates_from_original_anita_files(self, parser):
        worksheet = parser.parse_simple(3)
        for index, row in worksheet.iterrows():
            try:
                product_row = row['Style no.']
            except:
                product_row = row['Style no.\n*1)']

            price_row = row['R.R.P. RON']

            if is_nan(product_row) or price_is_not_as_expected(price_row):
                continue

            retail_price = UpdateStatement(self.info.table_name,
                                           self.info.column_name,
                                           price_row,
                                           self.info.code_name,
                                           str(product_row).split('\n'))
            parser.write_to_file(retail_price.generate_query())

    def create_special_price_updates(self, parser):
        worksheet = parser.parse_simple(0)
        for index, row in worksheet.iterrows():
            retail_price = UpdateStatement(self.info.table_name,
                                           self.info.column_name,
                                           row['Pret retail'],
                                           self.info.code_name,
                                           row['Cod'])
            parser.write_to_file(retail_price.generate_query_no_enumerate())

            special_price = UpdateStatement(self.info.table_name_special,
                                            self.info.column_name,
                                            row['Pret special 2023'],
                                            self.info.code_name,
                                            row['Cod'])
            parser.write_to_file(special_price.generate_join_query_for_sets())
